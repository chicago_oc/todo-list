# TodoList Application • [localhost](http://localhost:8080/login)

#### Приложение запускается в docker контейнере

- docker-compose [https://docs.docker.com/compose/install/]()

#### Клонировать проект

```
git clone git@bitbucket.org:chicago_oc/todo-list.git
```

#### Подтянуть зависимости
```
composer install
```

#### Из каталога проекта выполнить

```
docker-compose --build
```

#### Откройте в браузере
 - Вход - [http://localhost:8080/login]()
 - Регистрация - [http://localhost:8080/registration]()
 - Доступные списки - [http://localhost:8080/login]()



Created by [Davydov Denis](chicago.oop@gmail.com)
