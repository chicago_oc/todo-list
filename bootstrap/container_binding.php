<?php
/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 08.07.19
 */

use Illuminate\Container\Container;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * AuthService
 */
$container->bind(\App\Contracts\AuthServiceInterface::class, \App\Services\AuthService::class);
$container->singleton(\App\Services\AuthService::class);
$container->alias(\App\Services\AuthService::class,'auth');

/**
 * ListService
 */
$container->bind(\App\Contracts\ListServiceInterface::class, \App\Services\ListService::class);
$container->singleton(\App\Services\ListService::class);
$container->alias(\App\Services\ListService::class, 'todolist');

/**
 * AuthorizedInterface
 */
$container->bind(\App\Contracts\AuthorizedInterface::class, \App\Models\User::class);

/**
 * Response
 */
$container->alias(\App\Core\Response::class, 'response');
$container->bind(\App\Contracts\ResponseInterface::class, \App\Core\Response::class);

$container->singleton(\App\Core\Response::class, function(Container $c) {
    // заменять вместе с билдером
    $builder = new \App\Core\CookieBuilder();
    $service = new \Symfony\Component\HttpFoundation\Response();

    return new \App\Core\Response($service, $builder);
});

/**
 * Request
 */
$container->alias(\App\Core\Request::class, 'request');
$container->bind(\App\Contracts\RequestInterface::class, \App\Core\Request::class);
$container->singleton(\App\Core\Request::class, function(Container $c) {
    $request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();

    return new \App\Core\Request($request);
});

/**
 * View
 */
$container->alias(\App\Core\View::class, 'view');
$container->bind(\App\Contracts\ViewInterface::class, \App\Core\View::class);
$container->bind(\App\Core\View::class, function(Container $c) {
    $loader = new FilesystemLoader(view_path());
    $twig   = new Environment($loader);

    return new \App\Core\View($twig);
});

/**
 * Hash
 */
$container->alias(\App\Helpers\Hash::class, 'hash');

$container->bind(\App\Contracts\ApplicationInterface::class, \App\Core\Application::class);

$container->singleton(\Symfony\Component\Routing\Matcher\UrlMatcher::class, function (Container $c) {
    $fileLocator = new \Symfony\Component\Config\FileLocator([config_path()]);
    $loader = new \Symfony\Component\Routing\Loader\YamlFileLoader($fileLocator);
    $collections = $loader->load('routes.yaml');
    $request = $c->get(\App\Contracts\RequestInterface::class);
    $context = $c->makeWith(\Symfony\Component\Routing\RequestContext::class, [
        'baseUrl' => $request->getPath(),
        'method' => $request->getMethod(),
    ]);

    return new \Symfony\Component\Routing\Matcher\UrlMatcher($collections, $context);
});
