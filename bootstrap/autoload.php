<?php

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 */

ini_set('display_errors', 1);
error_reporting(E_ALL);

use Illuminate\Container\Container;

require_once dirname(__DIR__) . '/vendor/autoload.php';

Dotenv\Dotenv::create(__DIR__ . '/../')->load();

set_exception_handler(function($e) {
    (new \App\Core\ErrorHandler())->handle($e);
});

$manager = new Illuminate\Database\Capsule\Manager();
$manager->addConnection([
    'driver'    => env('DB_DRIVER'),
    'host'      => env('DB_HOST'),
    'database'  => env('DB_NAME'),
    'username'  => env('DB_USER'),
    'password'  => env('DB_PASSWORD'),
    'charset'   => env('DB_CHARSET'),
    'collation' => 'utf8_unicode_ci',
]);
$manager->bootEloquent();

$container = Container::getInstance();

require_once 'container_binding.php';

$application = $container->make(\App\Contracts\ApplicationInterface::class);
$response    = $application->run();
$response->send();
$application->terminate();