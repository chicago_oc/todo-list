<?php
/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 */

declare(strict_types=1);

require __DIR__ . '/../bootstrap/autoload.php';
