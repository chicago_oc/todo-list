/**
 * Made in a hurry
 */
(function(h, m, f) {
    var LIST_ID = $('#current-list-id').data('value');

    var request = function n(){
        this.getItems = function n(filter, callback) {
            $.ajax({
                url: '/api/items',
                method: 'GET',
                dataType: 'json',
                data: {
                    list_id: LIST_ID,
                    filter: filter
                },
                success: function (json) {
                    callback(filter, json);
                }
            });
        }
        this.clearCompleted = function (filter, callback) {
            $.ajax({
                url: '/api/items/completed',
                method: 'DELETE',
                dataType: 'json',
                data: {
                    list_id: LIST_ID
                },
                success: function(json) {
                    callback(filter, json);
                }
            });
        }
        this.updateItem = function(data, callback) {
            $.ajax({
                url: '/api/items',
                method: 'PUT',
                data: {
                    item_id: data.item_id,
                    status: data.status || null,
                    title: data.title || null
                },
                dataType: 'json',
                success: function(json) {
                    callback(data, json);
                }
            });
        }
        this.removeItem = function(data, callback) {
            $.ajax({
                url: '/api/items',
                method: 'DELETE',
                data: {
                    item_id: data.item_id
                },
                dataType: 'json',
                success: function() {
                    callback(data);
                }
            });
        }
        this.addItem = function(data, callback) {
            $.ajax({
                url: '/api/items',
                method: 'POST',
                data: {
                    list_id: LIST_ID,
                    title: data.title
                },
                dataType: 'json',
                success: function (json) {
                    callback(data, json);
                }
            })
        };
        this.toggleAll = function(data, callback) {
            $.ajax({
                url: '/api/items/toggle-all',
                method: 'PUT',
                data: {
                    list_id: LIST_ID,
                    status: data.status
                },
                dataType: 'json',
                success: function (json) {
                    callback(data, json);
                }
            });
        }

        return {
            getItems: getItems,
            clearCompleted: clearCompleted,
            updateItem: updateItem,
            removeItem: removeItem,
            addItem: addItem,
            toggleAll: toggleAll
        };
    }();

    var header = function(h) {
        this.el = $(h);
        this.el.on('keypress', '.new-todo', function(e) {
            $this = $(this);
            if (e.which != 13) {
                return;
            }
            var data = {
                title: $this.val(),
                edit: $this
            };
            request.addItem(data, function(data, json) {
                data.edit.val('');
                if (footer.filterValue() == 'completed') {
                    footer.items_length++;
                    footer.incActiveCount();
                    footer.render();
                    return;
                }
                body.appendItem(json);
                footer.render();
            });
        });
    }(h);

    var body = function(m){
        this.el = $(m);
        this.content = this.el.find('.todo-list');
        this.el.on('click', '.toggle', function() {
            var data = {
                item_id: $(this).closest('li').data('item-id'),
                status: $(this).prop('checked') ? 1 : 0,
                li: $(this).closest('li')
            };
            request.updateItem(data, function(data, json) {
                if(json.is_completed) {
                    data.li.addClass('completed');
                    footer.decActiveCount();
                    if (footer.filterValue() == 'active') {
                        data.li.remove();
                    }
                } else {
                    data.li.removeClass('completed');
                    footer.incActiveCount();
                    if (footer.filterValue() == 'completed') {
                        data.li.remove();
                    }
                }
                footer.render();
            });
        }).on('dblclick', '.todo-item-title', function() {
            var li = $(this).closest('li');
            li.addClass('editing');
            li.find('.edit').focus();
        }).on('blur', '.edit', function() {
            var li = $(this).closest('li');
            li.removeClass('editing');
        }).on('keypress', '.edit', function(e){
            var el = $(this),
                li = el.closest('li');
            if(e.which == 13) {
                var el_title = el.siblings('.view').find('.todo-item-title');
                el_title.text(el.val());
                var data = {
                    item_id: li.data('item-id'),
                    title: el.val(),
                    el_title: el_title
                };
                request.updateItem(data, function(data, json) {
                    data.el_title.text(json.title);
                });
                el.focusout();
            }
        }).on('click', '.destroy', function() {

            var li = $(this).closest('li'),
                data = {
                    item_id: li.data('item-id'),
                    li: li,
                    is_completed: li.hasClass('completed')
                };

            request.removeItem(data, function(data) {
                footer.items_length--;
                if (!data.is_completed) {
                    footer.decActiveCount();
                }
                data.li.remove();
                footer.render();
            });
        }).on('click', '.toggle-all', function() {

            var data = {
                status: $(this).prop('checked') ? 1 : 0
            };
            request.toggleAll(data, function(data, json) {
                footer.items_length = json.length;
                if (!data.status && footer.filterValue() == 'completed') {
                    json = [];
                } else if (data.status && footer.filterValue() == 'active') {
                    json = [];
                }
                renderItems(data.status, json);
                if (data.status) {
                    footer.setActiveCount(0);
                } else {
                    footer.setActiveCount(footer.items_length);
                }
                footer.render();
            });
        });
        var template_item = $('#todo-item-template');
        this.renderItems = function (filter, json) {
            var template = _.template(template_item.html());
            var html = template({items: json});

            content.html(html);
        }
        this.appendItem = function (json) {
            var template = _.template(template_item.html());

            content.append(template({items: [json]}));
            footer.incActiveCount();
            footer.items_length++;
            footer.render();
        }

        return {
            render: renderItems,
            appendItem: appendItem
        };
    }(m);

    var footer = function(f) {
        this.el = $(f);
        this.left_count = el.find('.todo-count strong');
        this.activeCount = parseInt(left_count.text());
        this.setActiveCount = function(count) {
            this.activeCount = count;
        };
        this.getActiveCount = function() {
            return this.activeCount;
        };
        this.incActiveCount = function() {
            this.activeCount++;
        };
        this.decActiveCount = function () {
            this.activeCount--;
        }

        this.btn_clear = el.find('.clear-completed');
        this.items_length = parseInt($('.footer').data('items-count'));
        this.filters = this.el.find('.filters');
        this.el.on('click', 'span', function () {
            filters.find('li span.selected').removeClass('selected');
            $(this).addClass('selected');
            request.getItems(filterValue(), body.render);
        }).on('click', '.clear-completed', function() {
            request.clearCompleted(filterValue(), function(filter, json) {
                footer.setActiveCount(json.length);
                footer.items_length = json.length;
                if (filter == 'completed') {
                    json = [];
                }
                body.render(filter, json);
                footer.render();
            });
        });

        this.filterValue = function() {
            return filters.find('li span.selected').data('filter');
        }

        this.render = function() {
            left_count.text(footer.getActiveCount());
            if ((items_length - footer.getActiveCount()) > 0){
                btn_clear.show();
            } else {
                btn_clear.hide();
            }
        }

        return this;
    }(f);

})('.header', '.main', '.footer');