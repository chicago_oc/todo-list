<?php

namespace App\Facades;

use App\Services\ListService as LS;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 */
class ListService
{
    protected static $service;

    protected static function instanceService(): ?LS
    {
        $container = \Illuminate\Container\Container::getInstance();

        return $container->get(LS::class);
    }

    public static function __callStatic($method, $args)
    {
        self::$service = self::instanceService();

        return self::$service->$method(...$args);
    }
}