<?php

namespace App\Facades;

use App\Services\AuthService;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 */
class Auth
{
    protected static $service;

    protected static function instanceService(): ?AuthService
    {
        $container = \Illuminate\Container\Container::getInstance();

        return $container->get(AuthService::class);
    }

    public static function __callStatic($method, $args)
    {
        self::$service = self::instanceService();

        return self::$service->$method(...$args);
    }
}