<?php

namespace App\Exceptions;

use Throwable;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 04.07.19
 */
class RegistrationException extends \RuntimeException
{
    public function __construct($message = "", $code = 400, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}