<?php

namespace App\Exceptions;

use Throwable;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 08.07.19
 */
class SomeException extends \RuntimeException
{
    public function __construct($message, $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}