<?php

namespace App\Exceptions;

use Throwable;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 08.07.19
 */
class MethodNotSupported extends \RuntimeException
{
    public function __construct($method, $code = 400, Throwable $previous = null)
    {
        parent::__construct(sprintf('Method [%s] is not supported.', $method), $code, $previous);
    }
}