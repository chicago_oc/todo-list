<?php

namespace App\Exceptions;

use Throwable;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 05.07.19
 */
class ModelNotFoundException extends \RuntimeException
{
    public function __construct($message = "", $code = 404, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}