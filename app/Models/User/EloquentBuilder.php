<?php

namespace App\Models\User;

use \Illuminate\Database\Eloquent\Builder;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 05.07.19
 */
class EloquentBuilder extends Builder
{
    public function withAccessToken($token): EloquentBuilder
    {
        return $this->whereNotNull('access_token')->where('access_token', $token);
    }

    public function withLogin($login)
    {
        return $this->where('login', $login);
    }
}