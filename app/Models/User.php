<?php

namespace App\Models;

use App\Contracts\AuthorizedInterface;
use App\Models\User\EloquentBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 *
 * @property int    $id
 * @property string $login
 * @property string $password
 * @property string $access_token
 */
class User extends Model implements AuthorizedInterface
{
    protected $table = 'users';

    protected $attributes = [
        'login'        => null,
        'password'     => null,
        'access_token' => null,
    ];

    protected $fillable = [
        'login',
        'password',
        'access_token',
    ];

    protected $casts = [
        'login'        => 'string',
        'password'     => 'string',
        'access_token' => 'string',
    ];

    protected $dates = [
        'created_at',
    ];

    const UPDATED_AT = null;

    /**
     * @param string $login
     *
     * @return \App\Contracts\AuthorizedInterface|null
     */
    public static function findByLogin($login): ?AuthorizedInterface
    {
        return static::query()->withLogin($login)->first();
    }

    /**
     * @param $password
     *
     * @return bool
     */
    public function checkPasswordHash($password): bool
    {
        return app('hash')::check($password, $this->password);
    }

    /**
     * @return bool
     */
    public function generateAccessToken(): bool
    {
        do {
            $this->access_token = Str::random(100);
        } while (User::query()->withAccessToken($this->access_token)->exists());

        return $this->save();
    }

    /**
     * @return bool
     */
    public function logout(): bool
    {
        $this->access_token = null;

        return $this->save();
    }

    /**
     * @param $token
     *
     * @return \App\Contracts\AuthorizedInterface|null
     */
    public static function findByAccessToken($token): ?AuthorizedInterface
    {
        return User::query()
            ->withAccessToken($token)
            ->first();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param \Illuminate\Database\Query\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|mixed
     */
    public function newEloquentBuilder($query)
    {
        return app(EloquentBuilder::class, ['query' => $query]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lists()
    {
        return $this->hasMany(TodoList::class, 'author_id', 'id');
    }
}