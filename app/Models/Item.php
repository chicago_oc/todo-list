<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 05.07.19
 *
 * @property int     $id
 * @property string  $title
 * @property boolean $is_completed
 *
 * @property \App\Models\TodoList $list
 */
class Item extends Model
{
    protected $table = 'list_items';

    protected $attributes = [
        'list_id'      => null,
        'title'        => null,
        'is_completed' => 0,
    ];

    protected $casts = [
        'title'        => 'string',
        'is_completed' => 'bool',
    ];

    protected $fillable = [
        'title',
        'is_completed',
    ];

    protected $hidden = [
        'list',
    ];

    public const UPDATED_AT = null;

    public function list(): BelongsTo
    {
        return $this->belongsTo(TodoList::class, 'list_id', 'id');
    }
}