<?php

namespace App\Models\TodoList;

use \Illuminate\Database\Eloquent\Builder;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 05.07.19
 */
class EloquentBuilder extends Builder
{
    public function withId($id): EloquentBuilder
    {
        return $this->where('id', $id);
    }

    public function withAuthId($id): EloquentBuilder
    {
        return $this->where('author_id', $id);
    }

    public function withIdAndAuthorId($listId ,$autorId): EloquentBuilder
    {
        return $this->where('id', $listId)->withAuthId($autorId);
    }
}