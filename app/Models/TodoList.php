<?php

namespace App\Models;

use App\Models\TodoList\EloquentBuilder;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 *
 * @property int    $id
 * @property string $title
 * @property int    $author_id
 *
 * @property \App\Models\User $author
 * @property EloquentCollection|Category[] $items
 */
class TodoList extends Model
{
    protected $table = 'lists';

    protected $attributes = [
        'title' => null,
        'author_id' => null,
    ];

    const UPDATED_AT = null;

    protected $casts = [
        'title' => 'string',
    ];

    protected $fillable = [
        'title',
        'author_id',
    ];

    protected $hidden = [
        'author',
    ];

    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class, 'author_id', 'id');
    }

    public function items(): HasMany
    {
        return $this->hasMany(Item::class, 'list_id', 'id');
    }

    public function newEloquentBuilder($query)
    {
        return new EloquentBuilder($query);
    }
}