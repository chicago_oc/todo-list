<?php

namespace App\Controllers\Api;

use App\Controllers\Controller;
use App\Exceptions\InvalidRequestParameterException;
use App\Exceptions\ModelNotFoundException;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 */
class ItemController extends Controller
{
    public function actionItems()
    {
        if ( ! ($listId = $this->request()->get('list_id'))) {
            throw new InvalidRequestParameterException('Parameters [list_id] is required.');
        }

        $filter = $this->request()->get('filter', 'all');
        $items  = app('todolist')->findItemsByListUserFilter($listId, $this->authService->getUserId(), $filter);

        return $this->response()->setJson($items);
    }

    public function actionStore()
    {
        if ( ! $this->request()->get('list_id') || ! $this->request()->get('title')) {
            throw new InvalidRequestParameterException('Parameters [list_id, title] is required.');
        }

        $list = app('todolist')->findByIdAndAuthId($this->request()->get('list_id'), $this->authService->getUserId());
        $item = app('todolist')->makeItemByTitle($this->request()->get('title'));
        $item = app('todolist')->addItemToList($item, $list);

        return $this->response()->setJson($item)->setStatusCode(200);
    }

    public function actionUpdate()
    {
        if ( null === ($itemId = $this->request()->get('item_id'))) {
            throw new InvalidRequestParameterException('Parameters [item_id] is required.');
        }

        $params = [];
        if ($title = $this->request()->get('title')) {
            $params['title'] = $title;
        }

        if (null !== ($status = $this->request()->get('status'))) {
            $params['is_completed'] = (int)$status;
        }

        $item = app('todolist')->itemFindById($itemId);

        if ( ! $item || ($item->list->author_id !== $this->authService->getUserId())) {
            throw new ModelNotFoundException(sprintf('Item [%s] not found', $itemId));
        }

        $item = app('todolist')->itemUpdateByParams($item, $params);

        return $this->response()->setJson($item);
    }

    public function actionDelete()
    {
        if ( null === ($itemId = $this->request()->get('item_id'))) {
            throw new InvalidRequestParameterException('Parameters [item_id] is required.');
        }

        $item = app('todolist')->itemFindById($itemId);

        if ( ! $item || ($item->list->author_id !== $this->authService->getUserId())) {
            throw new ModelNotFoundException('Item not found');
        }

        $item->delete();

        return $this->response()->setJson('')->setStatusCode(204);
    }

    public function actionClearCompleted()
    {
        if ( !($listId = $this->request()->get('list_id'))) {
            throw new InvalidRequestParameterException('Parameters [item_id] is required.');
        }

        /**
         * @var \App\Models\TodoList $list
         */
        $list = app('todolist')->findByIdAndAuthId($listId, $this->authService->getUserId());
        $list->items()->where('is_completed', 1)->delete();

        return $this->response()->setJson($list->items()->get());
    }

    public function actionToggleAll()
    {
        // todo простейший валидатор из настроек роутов!
        if ( !($listId = $this->request()->get('list_id'))) {
            throw new InvalidRequestParameterException('Parameters [list_id] is required.');
        }

        if (null === ($status = $this->request()->get('status'))) {
            throw new InvalidRequestParameterException('Parameters [status] is required.');
        }

        /**
         * @var \App\Models\TodoList $list
         */
        $list = app('todolist')->findByIdAndAuthId($listId, $this->authService->getUserId());

        $list->items()->where('is_completed', (int)!$status)->update([
            'is_completed' => (int)$status,
        ]);

        return $this->response()->setJson($list->items()->get());
    }
}