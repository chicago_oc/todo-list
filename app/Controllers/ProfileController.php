<?php

namespace App\Controllers;

use App\Exceptions\RegistrationException;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 */
class ProfileController extends Controller
{
    /**
     * @return \App\Contracts\ResponseInterface
     */
    public function actionRegistration()
    {
        $login     = $this->request()->get('login');
        $password  = $this->request()->get('password');
        $password2 = $this->request()->get('password2');

        if ($password !== $password2) {
            $this->addErrorMessage('Passwords not equals...');
        } elseif ($login && $password) {
            try {
                $this->authService->registration($login, $password);
            } catch (RegistrationException $e) {
                $this->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->addErrorMessage('Something went wrong when registration in');
            }

            if ( ! $this->hasErrors()) {
                return $this->response()->redirectTo('/login');
            }
        }

        return $this->render('registration.html.twig');
    }
}