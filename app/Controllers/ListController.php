<?php

namespace App\Controllers;

use App\Exceptions\InvalidRequestParameterException;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 04.07.19
 */
class ListController extends Controller
{
    /**
     * Доступные списки
     *
     * @return \App\Contracts\ResponseInterface
     */
    public function actionIndex()
    {
        if ($this->request()->getMethod() === 'POST') {
            if (!($title = $this->request()->get('title'))) {
                throw new InvalidRequestParameterException('Parameters [title] is required.');
            }

            app('todolist')->createByTitle($title);

            return $this->response()->redirectTo('/');
        }

        $lists = $this->authService->user()->lists;

        return $this->render('lists.index.html.twig', [
            'lists' => $lists,
            'user'  => app('auth')->check() ? app('auth')->user()->login : 'unauth',
        ]);
    }

    /**
     * Просмотр списка
     *
     * @return \App\Contracts\ResponseInterface
     */
    public function actionShow()
    {
        $listId = $this->request()->get('id');

        /**
         * @var \App\Models\TodoList $list
         */
        $list = app('todolist')->findByIdAndAuthId($listId, app('auth')->getUserId());
        app('todolist')->prepareItems($list);

        return $this->render('lists.details.html.twig', [
            'list'    => $list,
            'filters' => app('todolist')->filtersAvailable(),
        ]);
    }
}