<?php

namespace App\Controllers;

use App\Exceptions\UnauthorizedException;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 */
class AuthController extends Controller
{
    /**
     * @return \App\Contracts\ResponseInterface
     */
    public function actionLogin()
    {
        if ($this->authService->check()) {
            return $this->response()->redirectTo('/');
        }

        $login    = $this->request()->get('login');
        $password = $this->request()->get('password');

        if ($login && $password) {
            try {
                $this->authService->login($login, $password);

                return $this->response()
                    ->setCookie(env('USER_COOKIE_KEY'), app('auth')->user()->access_token,time() + env('USER_COOKIE_EXP'), '/')
                    ->redirectTo('/');

            } catch (UnauthorizedException $e) {
                $this->addErrorMessage('Invalid login or password');
            } catch (\Exception $e) {
                $this->addErrorMessage('Something went wrong when logging in');
            }
        }

        return $this->render('login.html.twig');
    }

    /**
     * @return \App\Contracts\ResponseInterface
     */
    public function actionLogout()
    {
        $this->authService->logout();

        return $this->response()
            ->redirectTo('/login')
            ->clearCookie(env('USER_COOKIE_KEY'));
    }
}