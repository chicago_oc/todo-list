<?php

namespace App\Controllers;

use App\Contracts\AuthServiceInterface;
use App\Contracts\ViewInterface;
use App\Contracts\RequestInterface;
use App\Contracts\ResponseInterface;
use App\Exceptions\MethodNotSupported;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 *
 * AuthService устанавливается через зависимости
 * ListService используется через хелпер
 * исключительно для примера
 */
class Controller
{
    private $request;
    private $response;
    private $view;
    private $errors = [];

    /**
     * @var \App\Contracts\AuthServiceInterface
     */
    protected $authService;

    /**
     * Controller constructor.
     *
     * @param \App\Contracts\AuthServiceInterface $authService
     * @param \App\Contracts\RequestInterface     $request
     * @param \App\Contracts\ResponseInterface    $response
     * @param \App\Contracts\ViewInterface        $view
     */
    public function __construct(AuthServiceInterface $authService, RequestInterface $request, ResponseInterface $response, ViewInterface $view)
    {
        $this->request     = $request;
        $this->response    = $response;
        $this->view        = $view;
        $this->authService = $authService;
    }

    /**
     * @return \App\Contracts\RequestInterface
     */
    protected function request(): RequestInterface
    {
        return $this->request;
    }

    /**
     * @return \App\Contracts\ResponseInterface
     */
    protected function response(): ResponseInterface
    {
        return $this->response;
    }

    /**
     * @param       $fileName
     * @param array $params
     *
     * @return \App\Contracts\ResponseInterface
     */
    protected function render($fileName, array $params = []): ResponseInterface
    {
        $params['errors'] = $this->errors;

        $content = $this->view->render($fileName, $params);

        return $this->response()->setContent($content);
    }

    /**
     * @param $method
     *
     * @return mixed
     */
    public function callAction($method)
    {
        return call_user_func_array([$this, $method], []);
    }

    /**
     * @param $method
     * @param $parameters
     */
    public function __call($method, $parameters)
    {
        throw new MethodNotSupported($method);
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return !empty($this->errors);
    }

    /**
     * @param string $message
     */
    protected function addErrorMessage(string $message)
    {
        $this->errors[] = $message;
    }
}