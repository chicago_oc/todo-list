<?php

namespace App\Core;

use App\Contracts\RequestInterface;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 *
 * Это адаптер над Symfony компонентом
 */
class Request implements RequestInterface
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request $request
     */
    private $service;

    /**
     * Request constructor.
     *
     * @param $service
     */
    public function __construct($service) {
        $this->service = $service;
        if (0 === strpos($this->service->headers->get('Content-Type'), 'application/json')) {
            $data = json_decode($this->service->getContent(), true);
            $this->service->request->replace(is_array($data) ? $data : array());
        }
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->service->getPathInfo();
    }

    /**
     * @param      $key
     * @param null $default
     *
     * @return mixed
     */
    public function get($key, $default = null)
    {
       return $this->service->get($key, $default);
    }

    /**
     * @param      $name
     * @param null $default
     *
     * @return mixed
     */
    public function getCookie($name, $default = null)
    {
        return $this->service->cookies->get($name, $default);
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function hasCookie($name): bool
    {
        return $this->service->cookies->has($name);
    }

    /**
     * @param $name
     *
     * @return string|string[]|null
     */
    public function getHeader($name)
    {
        return $this->service->headers->get($name);
    }

    /**
     * @return bool
     */
    public function isAjax(): bool
    {
        return $this->service->isXmlHttpRequest() || $this->getHeader('Content-Type') === 'application/json';
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->service->getMethod();
    }
}