<?php

namespace App\Core;

use App\Contracts\CookieBuilderInterface;
use App\Contracts\ResponseInterface;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 *
 * Это адаптер над Symfony компонентом
 */
class Response implements ResponseInterface
{
    /**
     * @var \Symfony\Component\HttpFoundation\Response $response
     */
    private $service;

    /**
     * @var \App\Contracts\CookieBuilderInterface
     */
    private $cookieBuilder;

    /**
     * Response constructor.
     *
     * @param                                       $service
     * @param \App\Contracts\CookieBuilderInterface $builder
     */
    public function __construct($service, CookieBuilderInterface $builder)
    {
        $this->service       = $service;
        $this->cookieBuilder = $builder;
    }

    /**
     * @param $url
     *
     * @return \App\Contracts\ResponseInterface
     */
    public function redirectTo($url): ResponseInterface
    {
        $this->service->headers->set('Location', $url);

        return $this;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function send()
    {
        return $this->service->send();
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sendHeaders()
    {
        return $this->service->sendHeaders();
    }

    /**
     * @param $content
     *
     * @return \App\Contracts\ResponseInterface
     */
    public function setContent($content): ResponseInterface
    {
        $this->service->setContent($content);

        return $this;
    }

    /**
     * @param string      $name
     * @param string|null $value
     * @param int         $expire
     * @param string|null $path
     *
     * @return \App\Contracts\ResponseInterface
     */
    public function setCookie(string $name, string $value = null, int $expire = 0, ?string $path = '/'): ResponseInterface
    {
        $cookie = $this->cookieBuilder->buildByParams($name, $value, $expire, $path);
        $this->service->headers->setCookie($cookie);

        return $this;
    }

    /**
     * @param string $name
     *
     * @return \App\Contracts\ResponseInterface
     */
    public function clearCookie(string $name): ResponseInterface
    {
        $this->service->headers->clearCookie($name);

        return $this;
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function hasHeader($name): bool
    {
        return $this->service->headers->has($name);
    }

    /**
     * @param      $name
     * @param      $value
     * @param bool $replace
     *
     * @return \App\Contracts\ResponseInterface
     */
    public function setHeader($name, $value, bool $replace = true): ResponseInterface
    {
        $this->service->headers->set($name, $value, $replace);

        return $this;
    }

    /**
     * @param int  $code
     * @param null $text
     *
     * @return \App\Contracts\ResponseInterface
     */
    public function setStatusCode(int $code, $text = null): ResponseInterface
    {
        $this->service->setStatusCode($code, $text);

        return $this;
    }

    /**
     * @param     $value
     * @param int $code
     *
     * @return \App\Contracts\ResponseInterface
     */
    public function setJson($value, int $code = 200): ResponseInterface
    {
        if ( ! is_string($value)) {
            $value = json_encode($value);
        }

        $this->setContent($value);
        $this->service->setStatusCode($code);

        if ( ! $this->hasHeader('Content-Type')) {
            $this->setHeader('Content-Type', 'application/json');
        }

        return $this;
    }
}