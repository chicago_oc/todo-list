<?php

namespace App\Core;

use App\Contracts\AuthServiceInterface;
use App\Contracts\MiddlewareInterface;
use App\Contracts\RequestInterface;
use App\Contracts\ResponseInterface;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 04.07.19
 */
abstract class Middleware implements MiddlewareInterface
{
    /**
     * @var \App\Contracts\AuthServiceInterface
     */
    protected $authService;

    /**
     * Middleware constructor.
     *
     * @param \App\Contracts\AuthServiceInterface $authService
     */
    public function __construct(AuthServiceInterface $authService)
    {
        $this->authService = $authService;
    }

    /**
     * @param \App\Contracts\RequestInterface  $request
     * @param \App\Contracts\ResponseInterface $response
     *
     * @return mixed
     */
    abstract public function handle(RequestInterface $request, ResponseInterface $response);
}