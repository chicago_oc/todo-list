<?php

namespace App\Core;

use App\Contracts\ApplicationInterface;
use App\Contracts\RequestInterface;
use App\Contracts\ResponseInterface;
use Symfony\Component\Routing\Matcher\UrlMatcher;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 08.07.19
 */
class Application implements ApplicationInterface
{
    /**
     * @var \Symfony\Component\Routing\Matcher\UrlMatcher
     */
    private $matcher;

    /**
     * @var \App\Contracts\RequestInterface
     */
    private $request;

    /**
     * @var \App\Contracts\ResponseInterface
     */
    private $response;

    /**
     * Application constructor.
     *
     * @param \Symfony\Component\Routing\Matcher\UrlMatcher $matcher
     * @param \App\Contracts\RequestInterface               $request
     * @param \App\Contracts\ResponseInterface              $response
     */
    public function __construct(UrlMatcher $matcher, RequestInterface $request, ResponseInterface $response)
    {
        $this->matcher  = $matcher;
        $this->request  = $request;
        $this->response = $response;
    }

    /**
     * @return \App\Contracts\ResponseInterface
     */
    public function run(): ResponseInterface
    {
        $params     = $this->matcher->match($this->request->getPath());
        $controller = app($params['_controller']);
        if ( ! empty($params['middleware'])) {
            foreach($params['middleware'] as $m) {
                $middleware = app($m);
                $middleware->handle($this->request, $this->response);
            }
        }

        return $controller->callAction($params['action']);
    }

    /**
     * Завершение приложения
     */
    public function terminate()
    {
        die();
    }
}