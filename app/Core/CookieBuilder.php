<?php

namespace App\Core;

use App\Contracts\CookieBuilderInterface;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 04.07.19
 */
class CookieBuilder implements CookieBuilderInterface
{
    /**
     * @param string      $name
     * @param string|null $value
     * @param int         $expire
     * @param string|null $path
     *
     * @return \Symfony\Component\HttpFoundation\Cookie
     */
    public function buildByParams(string $name, string $value = null, int $expire = 0, ?string $path = '/'): Cookie
    {
        return app(Cookie::class, [
            'name'   => $name,
            'value'  => $value,
            'expire' => $expire,
            'path'   => $path,
        ]);
    }
}