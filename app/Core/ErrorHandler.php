<?php

namespace App\Core;

use App\Exceptions\ModelNotFoundException;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 05.07.19
 */
class ErrorHandler
{
    /**
     * @param \Throwable $e
     */
    public function handle(\Throwable $e)
    {
        $code         = $e->getCode() ?: 500;
        $responseCode = $code;
        $message      = $e->getMessage();
        if ($code >= 500) {
            $responseCode = 500;
            if ( ! env('APP_DEBUG', false)) {
                $code = $responseCode;
                $message = 'Internal error';
            }
        }

        if (request()->isAjax()) {
            response()->setJson([
                'code'    => $code,
                'message' => $message,
            ])->setStatusCode($responseCode)->send();
        }

        if ($e instanceof ModelNotFoundException) {
            $content = app('view')->render('not-found.html.twig');
        } else {
            $content = app('view')->renderError($e);
        }

        response()->setContent($content)->send();
    }
}