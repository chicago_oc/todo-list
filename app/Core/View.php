<?php

namespace App\Core;

use App\Contracts\ViewInterface;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 04.07.19
 *
 * Это адаптер над Twig
 */
class View implements ViewInterface
{
    private $view;

    /**
     * View constructor.
     *
     * @param $service
     */
    public function __construct($service)
    {
        $this->view = $service;
    }

    /**
     * @param       $fileName
     * @param array $params
     *
     * @return string
     */
    public function render($fileName, array $params = [])
    {
        return $this->view->render($fileName, $params);
    }

    public function renderError($e)
    {
        return $this->render('error.html.twig', [
            'error' => $e,
        ]);
    }
}