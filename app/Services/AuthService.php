<?php

namespace App\Services;

use App\Contracts\AuthorizedInterface;
use App\Contracts\AuthServiceInterface;
use App\Exceptions\RegistrationException;
use App\Exceptions\SomeException;
use App\Exceptions\UnauthorizedException;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 */
class AuthService implements AuthServiceInterface
{
    private $userModel;

    private $user;

    /**
     * AuthService constructor.
     *
     * @param \App\Contracts\AuthorizedInterface $model
     */
    public function __construct(AuthorizedInterface $model)
    {
        $this->userModel = $model;
    }

    /**
     * @param $login
     * @param $password
     *
     * @return bool
     */
    public function login($login, $password): bool
    {
        /**
         * @var \App\Models\User $user
         */
        $user = $this->userModel->findByLogin($login);

        if (empty($user)) {
            throw new UnauthorizedException('Invalid login');
        }

        if ( ! $user->checkPasswordHash($password)) {
            throw new UnauthorizedException('Invalid password');
        }

        if ( ! $user->generateAccessToken()) {
            throw new SomeException('Something went wrong when logging');
        }

        $this->user = $user;

        return true;
    }

    /**
     * @param $value
     *
     * @return bool
     */
    public function findByToken($value): bool
    {
        if ($this->check()) {
            return true;
        }

        $user = $this->userModel->findByAccessToken($value);

        if ( ! $user) {
            return false;
        }

        $this->user = $user;

        return true;
    }

    /**
     * @return bool
     */
    public function check(): bool
    {
        return null !== $this->user;
    }

    /**
     * Logout method
     */
    public function logout()
    {
        if ($this->check()) {
            $this->user->logout();
            $this->user = null;
        }
    }

    /**
     * @return \App\Contracts\AuthorizedInterface|null
     */
    public function user(): ?AuthorizedInterface
    {
        return $this->user;
    }

    public function registration($login, $password): ?AuthorizedInterface
    {
        $user = $this->userModel
            ->findByLogin($login);

        if ($user) {
            throw new RegistrationException(sprintf('User with login [%s] is already exists...', $login));
        }

        $user = $this->userModel::create([
            'login'    => $login,
            'password' => app('hash')::make($password),
        ]);
        $user->save();

        return $user;
    }

    public function getUserId(): ?int
    {
        return $this->user()->getId() ?? null;
    }
}