<?php

namespace App\Services;

use App\Contracts\ListServiceInterface;
use App\Exceptions\ModelNotFoundException;
use App\Models\Item;
use App\Models\TodoList;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 */
class ListService implements ListServiceInterface
{
    /**
     * @var string
     */
    const FILTER_ALL = 'all';

    /**
     * @var string
     */
    const FILTER_ACTIVE = 'active';

    /**
     * @var string
     */
    const FILTER_COMPLETED = 'completed';

    /**
     * @param string $title
     *
     * @return \App\Models\TodoList
     */
    public function createByTitle(string $title): TodoList
    {
        $list = app(TodoList::class, ['attributes' => [
            'title' => $title,
        ]])->author()->associate(app('auth')->user());

        $list->save();

        return $list;
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getListsByAuthorId(int $id)
    {
        return TodoList::query()
            ->where('author_id', '=', $id)
            ->orderBy('id')
            ->get();
    }

    /**
     * @param int $id
     * @param int $authorId
     *
     * @return \App\Models\TodoList|null
     * @throws ModelNotFoundException
     */
    public function findByIdAndAuthId(int $id, int $authorId): TodoList
    {
        $list = TodoList::query()
            ->withIdAndAuthorId($id, $authorId)
            ->first();

        if ( ! $list) {
            throw new ModelNotFoundException(sprintf('List not found by id [%s]', $id), 402);
        }

        return $list;
    }

    /**
     * @param int         $id
     * @param int         $authorId
     * @param string|null $itemsFilter
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findItemsByListUserFilter(int $id, int $authorId, ?string $itemsFilter = null)
    {
        /**
         * @var TodoList $list
         */
        $list = TodoList::query()
            ->withIdAndAuthorId($id, $authorId)
            ->first();

        if ( ! $list) {
            throw new ModelNotFoundException(sprintf('List not found by id [%s]', $id), 402);
        }

        $items = $list->items()->orderBy('id')->get()->filter(function($item) use($itemsFilter) {
            switch ($itemsFilter) {
                case self::FILTER_ACTIVE:
                    return (false == $item->is_completed);
                case self::FILTER_COMPLETED:
                    return (true == $item->is_completed);
            }

            return true;
        });

        return $items->values();
    }

    /**
     * @param string $title
     *
     * @return \App\Models\Item
     */
    public function makeItemByTitle(string $title): Item
    {
        $item = app(Item::class, ['attributes' => [
            'title' => $title,
        ]]);

        return $item;
    }

    /**
     * @param \App\Models\Item     $item
     * @param \App\Models\TodoList $list
     *
     * @return \App\Models\Item
     */
    public function addItemToList(Item $item, TodoList $list): Item
    {
        $item->list()->associate($list);

        $item->save();

        return $item;
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed|object|null
     */
    public function itemFindById(int $id)
    {
        return Item::query()->find($id);
    }

    /**
     * @return array
     */
    public function filtersAvailable(): array
    {
        return [
            ListService::FILTER_ALL       => 'All',
            ListService::FILTER_ACTIVE    => 'Active',
            ListService::FILTER_COMPLETED => 'Completed',
        ];
    }

    /**
     * @param \App\Models\Item $item
     * @param array            $params
     *
     * @return \App\Models\Item
     */
    public function itemUpdateByParams(Item $item, array $params): Item
    {
        $item->fill($params);
        $item->save();

        return $item;
    }

    public function prepareItems(TodoList $list): TodoList
    {
        $items = $list->items()->orderBy('id')->get();
        $list->items = $items;

        return $list;
    }
}