<?php
namespace App\Contracts;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 04.07.19
 */
interface ResponseInterface
{
    public function redirectTo($url): ResponseInterface;

    public function send();

    public function sendHeaders();

    public function setContent($content): ResponseInterface;

    public function setCookie(string $name, string $value = null, int $expire = 0, ?string $path = '/'): ResponseInterface;

    public function clearCookie(string $name): ResponseInterface;

    public function hasHeader($name): bool;

    public function setHeader($name, $value, bool $replace = true): ResponseInterface;

    public function setJson($value): ResponseInterface;

    public function setStatusCode(int $code, $text = null): ResponseInterface;
}