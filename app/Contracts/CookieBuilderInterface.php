<?php

namespace App\Contracts;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 04.07.19
 */
interface CookieBuilderInterface
{
    public function buildByParams(string $name, string $value = null, int $expire = 0, ?string $path = '/');
}