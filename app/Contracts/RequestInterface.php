<?php

namespace App\Contracts;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 04.07.19
 */
interface RequestInterface
{
    public function get($key, $default = null);

    public function getCookie($name, $default = null);

    public function hasCookie($name): bool;

    public function isAjax(): bool;

    public function getMethod(): string;

    public function getPath();
}