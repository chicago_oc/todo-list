<?php

namespace App\Contracts;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 04.07.19
 */
interface AuthServiceInterface
{
    public function login($login, $password): bool;

    public function findByToken($token): bool;

    public function check(): bool;

    public function user(): ?AuthorizedInterface;

    public function registration($login, $password): ?AuthorizedInterface;

    public function logout();

    public function getUserId(): ?int;
}