<?php

namespace App\Contracts;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 */
interface AuthorizedInterface
{
    public static function findByLogin($login): ?AuthorizedInterface;

    public static function findByAccessToken($token): ?AuthorizedInterface;

    public function checkPasswordHash($hash): bool;

    public function generateAccessToken(): bool;

    public function logout(): bool;

    public function getId(): ?int;
}