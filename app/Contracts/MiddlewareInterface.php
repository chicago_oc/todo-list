<?php

namespace App\Contracts;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 */
interface MiddlewareInterface {
    public function handle(RequestInterface $request, ResponseInterface $response);
}