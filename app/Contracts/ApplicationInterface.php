<?php

namespace App\Contracts;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 08.07.19
 */
interface ApplicationInterface
{
    public function run(): ResponseInterface;

    public function terminate();
}