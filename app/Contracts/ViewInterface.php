<?php

namespace App\Contracts;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 04.07.19
 */
interface ViewInterface
{
    public function render($fileName, array $params = []);

    public function renderError($e);
}