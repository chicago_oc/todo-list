<?php

namespace App\Contracts;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 04.07.19
 */
interface ListServiceInterface
{
    public function getListsByAuthorId(int $id);
}