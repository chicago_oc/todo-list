<?php

namespace App\Helpers;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 05.07.19
 */
class Hash
{
    /**
     * @param $password
     * @param $hash
     *
     * @return bool
     */
    public static function check($password, $hash)
    {
        list(, $salt) = explode(':', $hash);

        return ($hash == self::make($password, $salt));
    }

    /**
     * @param      $value
     * @param null $salt
     *
     * @return string
     */
    public static function make($value, $salt = null)
    {
        if (empty($salt)) {
            $salt = \Illuminate\Support\Str::random(10);
        }

        return md5($value . '::' . $salt) . ':' . $salt;
    }
}