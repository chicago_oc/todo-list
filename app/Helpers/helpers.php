<?php
/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 */

use Illuminate\Container\Container;

if (! function_exists('app')) {

    function app($abstract = null, array $parameters = [])
    {
        if (is_null($abstract)) {
            return Container::getInstance();
        }

        return empty($parameters)
            ? Container::getInstance()->make($abstract)
            : Container::getInstance()->makeWith($abstract, $parameters);
    }
}

if (! function_exists('view')) {

    function view($file, array $params)
    {
        return app('view')->render($file, $params);
    }
}

if (! function_exists('response')) {
    function response()
    {
        return app('response');
    }
}

if (! function_exists('request')) {
    function request()
    {
        return app('request');
    }
}

if (! function_exists('redirect')) {
    function redirect($to)
    {
        return response()->redirectTo($to);
    }
}
if ( ! function_exists('value'))
{
    /**
     * Return the default value of the given value.
     *
     * @param  mixed  $value
     * @return mixed
     */
    function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
}
if ( ! function_exists('env'))
{
    function env($key, $default = null)
    {
        $value = getenv($key);

        if ($value === false) return value($default);

        switch (strtolower($value))
        {
            case 'true':
            case '(true)':
                return true;

            case 'false':
            case '(false)':
                return false;

            case 'empty':
            case '(empty)':
                return '';

            case 'null':
            case '(null)':
                return;
        }

        return $value;
    }
}

if (! function_exists('app_path')) {
    function app_path()
    {
        return __DIR__ . '/../../';
    }
}

if (! function_exists('view_path')) {
    function view_path()
    {
        return app_path() . env('VIEW_DIR') . DIRECTORY_SEPARATOR;
    }
}

if (! function_exists('config_path')) {
    function config_path()
    {
        return app_path() . env('CONFIG_DIR') . DIRECTORY_SEPARATOR;
    }
}