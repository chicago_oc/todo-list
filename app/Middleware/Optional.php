<?php

namespace App\Middleware;

use App\Contracts\RequestInterface;
use App\Contracts\ResponseInterface;
use App\Core\Middleware;

/**
 * @author Davydov Denis <chicago.oop@gmail.com>
 *
 * Created at 03.07.19
 */
class Optional extends Middleware
{
    /**
     * @param \App\Contracts\RequestInterface  $request
     * @param \App\Contracts\ResponseInterface $response
     *
     * @return mixed|void
     */
    public function handle(RequestInterface $request, ResponseInterface $response)
    {
        if ( ! $request->hasCookie(env('USER_COOKIE_KEY'))) {
            return;
        }

        $accessToken = $request->getCookie(env('USER_COOKIE_KEY'));
        if ( ! $this->authService->findByToken($accessToken)) {
            $response
                ->redirectTo('/login')
                ->clearCookie(env('USER_COOKIE_KEY'))
                ->sendHeaders();
        }
    }
}